import getpass
import os

import bcrypt

from utils import connect_to_database


def is_root_or_sudoer():
    """
    Check if user is root or sudoer
    :return:
    """
    return os.geteuid() == 0 or 'SUDO_USER' in os.environ


def ask_password():
    """
    Ask for password
    :return:
    :rtype: bool
    """
    user_password = getpass.getpass("Enter your password: ")
    conn = connect_to_database()
    cursor = conn.cursor()
    query = "SELECT salt, token FROM users"
    cursor.execute(query)
    data = cursor.fetchall()
    cursor.close()
    conn.close()
    if data:
        stored_hashed_password = data[0][1].encode()
        if bcrypt.checkpw(user_password.encode(), stored_hashed_password):
            return True
        else:
            return False
    else:
        print("No user data found in the database.")
        return False
