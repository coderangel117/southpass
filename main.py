from security import is_root_or_sudoer, ask_password
from utils import connect_to_database


def main():
    if is_root_or_sudoer():
        connect_to_database()
        print("Connected to database")
        if ask_password():
            print("Password accepted")
        else:
            print("Password rejected")
            exit(1)
    else:
        print("Ce script nécessite des privilèges sudo.")
        exit(1)


if __name__ == "__main__":
    main()
