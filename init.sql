CREATE DATABASE southpass;

\c southpass;

CREATE TABLE users
(
    id    serial primary key,
    salt  text not null,
    token text not null,
    unique (token)
);

CREATE TABLE CredentialsTypes
(
    id    serial primary key,
    label varchar(35) not null
);

CREATE TABLE credentials
(
    id           serial primary key,
    title        char(32)  null,
    organisation char(32)  null,
    login        char(32)  null,
    email        char(50)  null,
    password     char(150) null,
    url          char(32)  null,
    user_id      int       not null,
    type_id      int       not null,
    foreign key (user_id) references users (id),
    foreign key (type_id) references CredentialsTypes (id)
);

-- Create a first user (temporary)
-- password is 123456
insert into users (salt, token)
values ('$2b$12$CPCwogiGxtnHkXlxPAl4He', '$2b$12$CPCwogiGxtnHkXlxPAl4HeVm4dozxCGX2wCDt9PvPsKM.Sj46881G');