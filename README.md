# southpass

A cool password manager written in Python.

# Visuals

Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see
GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

![manage.png](images/manage_item.png)

# Installation

Please consider you must Python 3.6+ and Docker in order to run southpass.

## Pulling the official postgres image

```bash
docker pull postgres:latest
```

## Running southpass

```bash
docker run --name postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=postgres -p 5432:5432  -v ./init.sql:/docker-entrypoint-initdb.d/init.sql -d postgres
```

Verify that it is running by running the following command:

```bash
docker ps
```

# Support

Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address,
etc.

# Roadmap

If you have ideas for releases in the future, it is a good idea to list them in the README.
