import os

import psycopg2
from dotenv import load_dotenv

load_dotenv()
db_host = os.getenv("DB_HOST")
db_name = os.getenv("DB_NAME")
db_user = os.getenv("DB_USER")
db_passwd = os.getenv("DB_PASSWD")


def connect_to_database():
    """
    Connect to PostgreSQL database
    :return: Connexion à la base de données PostgreSQL
    :rtype: psycopg2.extensions.connection
    """
    try:
        conn = psycopg2.connect(
            host=db_host,
            user=db_user,
            password=db_passwd,
            database=db_name,
            connect_timeout=3
        )
        return conn
    except Exception as e:
        print(e)
